create table account
(
    id               int auto_increment
        primary key,
    email            varchar(50) charset utf8mb3 not null,
    passwordHash     varchar(64)                 not null,
    isAdmin          tinyint(1)                  not null,
    username         varchar(50)                 not null,
    firstname        varchar(50) charset utf8mb3 null,
    lastname         varchar(45) charset utf8mb3 null,
    isVerified       tinyint(1)                  not null,
    createdOn        datetime(3)                 not null,
    updatedOn        datetime(3)                 not null,
    verifyKey        varchar(64)                 not null,
    verifyExpire     datetime(3)                 not null,
    changePassKey    varchar(64)                 not null,
    changePassExpire datetime(3)                 not null,
    balance          float                       not null,
    constraint email_UNIQUE
        unique (email)
);

create table account_auth
(
    id         int auto_increment
        primary key,
    selector   varchar(12) not null,
    validator  varchar(64) not null,
    account_id int         not null,
    constraint account_id
        foreign key (account_id) references account (id)
);

create table cart
(
    id        int auto_increment
        primary key,
    createdBy int         null,
    isDeleted tinyint(1)  null,
    createdAt datetime(3) null,
    updatedAt datetime(3) null,
    constraint id_UNIQUE
        unique (id),
    constraint createdBy
        foreign key (createdBy) references account (id)
);

create index createdBy_idx
    on cart (createdBy);

create table supplier
(
    id    int          not null
        primary key,
    name  varchar(60)  null,
    image varchar(200) null
);

create table order_processing
(
    id         int auto_increment
        primary key,
    supplierId int not null,
    price      int not null,
    quantity   int not null,
    accountId  int not null,
    constraint order_processing_account_id_fk
        foreign key (accountId) references account (id),
    constraint order_processing_supplier_id_fk
        foreign key (supplierId) references supplier (id)
);

create table product
(
    id          int auto_increment
        primary key,
    title       varchar(100)   null,
    description varchar(10000) null,
    price       float          null,
    quantity    int            null,
    createdBy   int            null,
    supplier    int            null,
    constraint title_UNIQUE
        unique (title),
    constraint supplier
        foreign key (supplier) references supplier (id),
    constraint userid
        foreign key (createdBy) references account (id)
)
    comment '	';

create table cart_item
(
    id          int auto_increment
        primary key,
    product_id  int         null,
    cart_id     int         null,
    productName varchar(65) null,
    quantity    int         null,
    isDeleted   tinyint(1)  null,
    createdBy   varchar(65) null,
    createdAt   datetime(3) null,
    updatedAt   datetime(3) null,
    constraint cart_id
        foreign key (cart_id) references cart (id),
    constraint productId
        foreign key (product_id) references product (id)
);

create index cart_id_idx
    on cart_item (cart_id);

create index productId_idx
    on cart_item (product_id);

create table `order`
(
    id         int auto_increment
        primary key,
    product_id int           not null,
    createdBy  int           not null,
    productLog varchar(2000) not null,
    totalPrice float         not null,
    constraint created_by
        foreign key (createdBy) references account (id),
    constraint order_product_id_fk
        foreign key (product_id) references product (id)
);

create index created_by_idx
    on `order` (createdBy);

create table order_item
(
    id         int auto_increment
        primary key,
    product_id int   null,
    price      float null,
    discount   float null,
    quantity   int   null,
    constraint order_item_product_id_fk
        foreign key (product_id) references product (id)
);

create index supplier_idx
    on product (supplier);

create index userid_idx
    on product (createdBy);

create table product_detail
(
    id                int auto_increment
        primary key,
    product_id        int         not null,
    seriNumber        varchar(45) not null,
    code              varchar(45) not null,
    productExpireDate datetime(3) not null,
    constraint id_UNIQUE
        unique (id),
    constraint product_id
        foreign key (product_id) references product (id)
);

create index product_id_idx
    on product_detail (product_id);

create definer = root@`%` trigger delete_product_quantity
    after delete
    on product_detail
    for each row
BEGIN
  UPDATE product
  SET quantity = (SELECT COUNT(product_id) FROM product_detail WHERE product_id = OLD.product_id)
  WHERE id = OLD.product_id;
END;

create definer = root@`%` trigger insert_product_quantity
    after insert
    on product_detail
    for each row
BEGIN
  UPDATE product
  SET quantity = (SELECT COUNT(product_id) FROM product_detail WHERE product_id = NEW.product_id)
  WHERE id = NEW.product_id;
END;

create definer = root@`%` trigger update_product_quantity
    after update
    on product_detail
    for each row
BEGIN
  UPDATE product
  SET quantity = (SELECT COUNT(product_id) FROM product_detail WHERE product_id = NEW.product_id)
  WHERE id = NEW.product_id;
END;

