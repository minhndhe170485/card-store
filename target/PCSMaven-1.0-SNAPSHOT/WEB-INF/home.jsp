<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%-- 
    Document   : home
    Created on : May 14, 2023, 2:43:21 AM
    Author     : Minh Nguyen
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <title>Phone Card Store</title>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"></script>
        <script src="https://kit.fontawesome.com/59b37168e9.js" crossorigin="anonymous"></script>
        <script src="/js/myjs.js"></script>
        <link rel="stylesheet" href="/css/mycss.css"/>
        <script>
            var processingNoti = "<div class=\"notification alert alert-" + "info" + " alert-dismissible fade show\">" +
                    "<button type=\"button\" class=\"btn-close\" data-bs-dismiss=\"alert\"></button>" +
                    "Processing your order..." +
                    "</div>";
            function invalidInputNoti(message) {
                return "<div class=\"notification alert alert-" + "danger" + " alert-dismissible fade show\">" +
                        "<button type=\"button\" class=\"btn-close\" data-bs-dismiss=\"alert\"></button>" +
                        message +
                        "</div>";
            }
            var loadingSupList = "<li class=\"line\" style=\"width: 150px; height:85px;\"></li><li class=\"line\" style=\"width: 150px; height:85px;\"></li><li class=\"line\" style=\"width: 150px; height:85px;\"></li><li class=\"line\" style=\"width: 150px; height:85px;\"></li>";
            var loadingPriceList = "<li class=\"line\" style=\"width: 150px; height:50px; border:none;\"></li><li class=\"line\" style=\"width: 150px; height:50px; border:none;\"></li><li class=\"line\" style=\"width: 150px; height:50px; border:none;\"></li><li class=\"line\" style=\"width: 150px; height:50px; border:none;\"></li>";
            $(document).ready(function () {
                $("#supList").html(loadingSupList);
                $("#buyBtnContainer").html(null);
                $.get("/api/card", {requestStr: "supList"}, function (responseJSON) {
                    $("#supList").html(null);
                    var len = responseJSON.length;
                    for (var i = 0; i < len; i++)
                    {
                        var li_str = "<li><a id=\"" + responseJSON[i].id + "\"><img src=\"" + responseJSON[i].image + "\" alt=\"" + responseJSON[i].name + "\"></a></li>";
                        $("#supList").append(li_str);
                    }
                });
                $('#txtCardTitle').addClass("spinner-grow");
                $('#txtCardDescription').addClass("spinner-grow");
                $('#txtCardProvider').addClass("spinner-grow");
                $('#txtCardPrice').addClass("spinner-grow");
                $('#txtCardQuantity').addClass("spinner-grow");
                $('#txtTotalPrice').addClass("spinner-grow");
            });
            $(document).on("click", "#supList li", function () {
                $("#supList .active").removeClass("active");
                $("#buyBtnContainer").html(null);
                $(this).addClass("active");
                $("#priceList").html(loadingPriceList);
                $.get("/api/card", {requestStr: "priceList", id: $(this).find("a").attr("id")}, function (responseJSON) {
                    $("#priceList").html(null);
                    var len = responseJSON.length;
                    for (var i = 0; i < len; i++)
                    {
                        var li_str = "<li><a id=\"" + responseJSON[i] + "\">" + responseJSON[i] + "₫</a></li>";
                        $("#priceList").append(li_str);
                    }
                    $("#priceList li").addClass("bg-white");
                });
                $("#buyBtnContainer").html(null);
                $("#cancelBtnContainer").html(null);
                $("#priceList").html(null);
                $("#quantity").attr({
                    "max": "",
                    "min": "",
                    "value": ""
                });
                $("#quantity").val(null);
                $('#quantity').prop('disabled', true);
                $('#confirmBtn').prop('disabled', true);
                $("#stockNumber").text(null);
                $("#txtCardTitle").text(null).addClass("spinner-grow");
                $("#txtCardDescription").text(null).addClass("spinner-grow");
                $("#txtCardPrice").text(null).addClass("spinner-grow");
                $("#txtCardProvider").text(null).addClass("spinner-grow");
                $("#txtCardQuantity").text(null).addClass("spinner-grow");
                $("#txtTotalPrice").text(null).addClass("spinner-grow");
            });
            $(document).on("click", "#priceList li", function () {
                $("#priceList .active").removeClass("active");
                $(this).addClass("active");
                $.get("/api/card", {requestStr: "quantity", price: $(this).find("a").attr("id"), supId: $("#supList .active").find("a").attr("id")}, function (response) {
                    $("#buyBtnContainer").html(null);
                    $("#cancelBtnContainer").html(null);
                    $("#txtCardTitle").text(null).addClass("spinner-grow");
                    $("#txtCardDescription").text(null).addClass("spinner-grow");
                    $("#txtCardPrice").text(null).addClass("spinner-grow");
                    $("#txtCardProvider").text(null).addClass("spinner-grow");
                    $("#txtCardQuantity").text(null).addClass("spinner-grow");
                    $("#txtTotalPrice").text(null).addClass("spinner-grow");
                    $('#quantity').prop('disabled', false);
                    $('#confirmBtn').prop('disabled', false);
                    $("#stockNumber").text(null);
                    $("#quantity").attr({
                        "max": response,
                        "min": response === "0" ? "0" : "1",
                        "value": response === "0" ? "" : "1"
                    });
                    if (response === "0")
                    {
                        $("#quantity").val(null);
                        $('#quantity').prop('disabled', true);
                        $('#confirmBtn').prop('disabled', true);
                        $("#buyBtnContainer").html(null);
                    } else
                    {
                        $("#quantity").val("1");
                    }
                    $("#stockNumber").text(response + " in stock");
                });
            });
            $(document).on("click", "#buyBtn", function () {
                $("#notificationDiv").html(processingNoti);
                $('#buyBtn').prop('disabled', true);
                $('#loadingSpan').addClass("spinner-border").addClass("spinner-border-sm");
                $.get("/api/buy", {price: $("#priceList .active").find("a").attr("id"), supId: $("#supList .active").find("a").attr("id"), quantity: $("#quantity").val()}, function (responseHTML) {
                    $('#loadingSpan').removeClass("spinner-border").removeClass("spinner-border-sm");
                    $("#notificationDiv").html(responseHTML);
                    $("#buyBtnContainer").html(null);
                    $("#cancelBtnContainer").html(null);
                    $("#priceList").html(null);
                    $("#quantity").attr({
                        "max": "",
                        "min": "",
                        "value": ""
                    });
                    $("#quantity").val(null);
                    $('#quantity').prop('disabled', true);
                    $('#confirmBtn').prop('disabled', true);
                    $("#stockNumber").text(null);
                    $("#txtCardTitle").text(null).addClass("spinner-grow");
                    $("#txtCardDescription").text(null).addClass("spinner-grow");
                    $("#txtCardPrice").text(null).addClass("spinner-grow");
                    $("#txtCardProvider").text(null).addClass("spinner-grow");
                    $("#txtCardQuantity").text(null).addClass("spinner-grow");
                    $("#txtTotalPrice").text(null).addClass("spinner-grow");
                    $("#supList .active").removeClass("active");
                });
            });
            $(document).on("click", "#cancelBtn", function () {
                $("#buyBtnContainer").html(null);
                $("#cancelBtnContainer").html(null);
                $("#priceList").html(null);
                $("#quantity").attr({
                    "max": "",
                    "min": "",
                    "value": ""
                });
                $("#quantity").val(null);
                $('#quantity').prop('disabled', true);
                $('#confirmBtn').prop('disabled', true);
                $("#stockNumber").text(null);
                $("#txtCardTitle").text(null).addClass("spinner-grow");
                $("#txtCardDescription").text(null).addClass("spinner-grow");
                $("#txtCardPrice").text(null).addClass("spinner-grow");
                $("#txtCardProvider").text(null).addClass("spinner-grow");
                $("#txtCardQuantity").text(null).addClass("spinner-grow");
                $("#txtTotalPrice").text(null).addClass("spinner-grow");
                $("#supList .active").removeClass("active");
            });
            $(document).on("click", "#confirmBtn", function () {
                if (!$("#quantity").val())
                {
                    $("#notificationDiv").html(invalidInputNoti("Input must not be blank."));
                } else if (parseInt($("#quantity").val()) < parseInt($("#quantity").attr("min")))
                {
                    $("#notificationDiv").html(invalidInputNoti("Quantity must be at least " + $("#quantity").attr("min") + "."));
                } else if (parseInt($("#quantity").val()) > parseInt($("#quantity").attr("max")))
                {
                    $("#notificationDiv").html(invalidInputNoti("Quantity must not be greater than " + $("#quantity").attr("max") + "."));
                } else
                {
                    $.get("/api/card", {requestStr: "cardInfo", price: $("#priceList .active").find("a").attr("id"), supId: $("#supList .active").find("a").attr("id")}, function (responseJSON) {
                        $("#buyBtnContainer").html("<button type=\"button\" id=\"buyBtn\" class=\"btn btn-primary mt-3\">Buy <span id ='loadingSpan' class=\"\"></span></button>");
                        $("#cancelBtnContainer").html("<button type=\"button\" id=\"cancelBtn\" class=\"btn btn-danger mt-3\">Cancel</button>");
                        $("#txtCardTitle").removeClass("spinner-grow").text(responseJSON.title).removeClass("spinner-grow");
                        $("#txtCardDescription").removeClass("spinner-grow").text(responseJSON.description);
                        $("#txtCardPrice").removeClass("spinner-grow").text(responseJSON.price + "₫");
                        $("#txtCardProvider").removeClass("spinner-grow").text(responseJSON.supplier);
                        $("#txtCardQuantity").removeClass("spinner-grow").text($("#quantity").val());
                        $("#txtTotalPrice").removeClass("spinner-grow").text($("#quantity").val() * responseJSON.price + "₫");
                    });
                }
            });
        </script>
    </head>
    <body>
        <div class="modal fade" id="myModal">
            <div class="modal-dialog">
                <div class="modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">Confirm Buying</h4>
                        <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body">
                        <p id="modalText">Proceed to buy ?</p>
                    </div>

                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light" data-bs-dismiss="modal">Cancel</button>
                        <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Buy</button>
                    </div>

                </div>
            </div>
        </div>
        <nav class="navbar navbar-expand-sm bg-dark navbar-dark" id="navbar">
            <div class="container">
                <a class="navbar-brand" href="/">Phone Card Store</a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#collapsibleNavbar">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="collapsibleNavbar">
                    <ul class="navbar-nav">
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown">PROVIDER</a>
                            <ul class="dropdown-menu">
                                <li><a class="dropdown-item" href="#">Viettel</a></li>
                                <li><a class="dropdown-item" href="#">Vinaphone</a></li>
                                <li><a class="dropdown-item" href="#">MobiFone</a></li>
                                <li><a class="dropdown-item" href="#">Vietnamobile</a></li>
                            </ul>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown">PRICE</a>
                            <ul class="dropdown-menu">
                                <li><a class="dropdown-item" href="#">10.000₫</a></li>
                                <li><a class="dropdown-item" href="#">20.000₫</a></li>
                                <li><a class="dropdown-item" href="#">50.000₫</a></li>
                                <li><a class="dropdown-item" href="#">100.000₫</a></li>
                                <li><a class="dropdown-item" href="#">200.000₫</a></li>
                                <li><a class="dropdown-item" href="#">500.000₫</a></li>
                            </ul>
                        </li>
                    </ul>
                    <ul class="navbar-nav ms-auto">
                        <c:if test="${not empty sessionScope.account}">
                            <span class="navbar-text me-1">${sessionScope.account.getBalance()}₫</span>
                        </c:if>
                        <c:if test="${sessionScope.account.getIsAdmin()}">
                            <li class="nav-item">
                                <a class="nav-link" href="/admin"><i class="fa-solid fa-gear"></i></i></a>
                            </li>
                        </c:if>
                        <li class="nav-item">
                            <a class="nav-link" href="/profile"><i class="fa-solid fa-circle-user"></i></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/orders"><i class="fa-solid fa-cart-shopping"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <main class="mb-4">
            <section class="container mt-5">
                <div class="row">
                    <div class="col-md-6">
                        <div class="p-4 bg-light shadow rounded">
                            <h3 class="mb-4 text-center">Choose Provider</h3>
                            <ul class="provider__list-container" id="supList">

                            </ul>
                        </div>
                        <div class="mt-4 p-4 bg-light shadow rounded">
                            <h3 class="mb-4 text-center">Choose Price</h3>
                            <ul class="price__list-container" id="priceList">

                            </ul>
                        </div>
                        <div class="mt-4 p-4 bg-light shadow rounded">
                            <h3 class="mb-4 text-center">Choose Quantity</h3>
                            <div class="input-group mb-1"> 
                                <input type="number" id="quantity" class="form-control" style="width: 200px; display: inline" disabled>
                                <button type="button" id="confirmBtn" class="btn btn-success" disabled>Confirm</button>
                            </div>
                            <span id="stockNumber" ></span>
                        </div>
                    </div>
                    <div class="col-md-6 bg-light shadow rounded p-4">
                        <h1 class="mb-5">Product information</h1>
                        <h3 class="mb-4" style = "display: flex; justify-content: space-between;"><span>Title</span> <span class="fw-light" id="txtCardTitle"></h3>
                        <h3 class="mb-4" style = "display: flex; justify-content: space-between;"><span>Description</span> <span class="fw-light" id="txtCardDescription"></h3>
                        <h3 class="mb-4" style = "display: flex; justify-content: space-between;"><span>Provider</span> <span class="fw-light" id="txtCardProvider"></h3>
                        <h3 class="mb-4" style = "display: flex; justify-content: space-between;"><span>Price</span> <span class="fw-light" id="txtCardPrice"></h3>
                        <h3 class="mb-4" style = "display: flex; justify-content: space-between;"><span>Quantity</span> <span class="fw-light" id="txtCardQuantity"></span></h3>
                        <h3 class="mb-4" style = "display: flex; justify-content: space-between;"><span>Total</span> <span class="fw-light" id="txtTotalPrice"></span></h3>
                            <c:choose>
                                <c:when test="${not empty sessionScope.account}">
                                    <c:choose>
                                        <c:when test="${sessionScope.account.getIsVerified()}">
                                            <div class="row m-0">
                                                <div id="buyBtnContainer" class="col me-1 d-grid">

                                                </div>
                                                <div id="cancelBtnContainer" class="col ms-1 d-grid">

                                                </div>
                                            </div>
                                        </c:when>
                                        <c:otherwise>
                                            <h3 class="mt-3">Verify your account to continue</h3>
                                        </c:otherwise>
                                    </c:choose>
                                </c:when>
                                <c:otherwise>
                                    <h3 class="mt-3">Login to continue</h3>
                                </c:otherwise>
                            </c:choose>
                    </div>
                </div>
            </section>
        </main>
        <footer class="text-center text-lg-start bg-light text-muted">
            <section class="d-flex justify-content-center justify-content-lg-between p-4 border-bottom">
                <div class="me-5 d-none d-lg-block">
                    <span>Get connected with us on social networks:</span>
                </div>
                <div>
                    <a href="https://www.facebook.com/duc.minh.2911/" target="_blank" class="me-4 text-reset">
                        <i class="fab fa-facebook-f"></i>
                    </a>
                </div>
            </section>
            <section class="">
                <div class="container text-center text-md-start mt-5">
                    <div class="row mt-3">
                        <div class="col-md-3 col-lg-4 col-xl-3 mx-auto mb-4">
                            <h6 class="text-uppercase fw-bold mb-4">
                                <i class="fas fa-gem me-3"></i>Phone Card Store
                            </h6>
                            <p>
                                Made by group 6.
                            </p>
                        </div>
                        <div class="col-md-2 col-lg-2 col-xl-2 mx-auto mb-4">
                            <h6 class="text-uppercase fw-bold mb-4">
                                Provider
                            </h6>
                            <p>
                                <a href="#!" class="text-reset">Viettel</a>
                            </p>
                            <p>
                                <a href="#!" class="text-reset">Vinaphone</a>
                            </p>
                            <p>
                                <a href="#!" class="text-reset">MobiFone</a>
                            </p>
                            <p>
                                <a href="#!" class="text-reset">Vietnamobile</a>
                            </p>
                        </div>
                        <div class="col-md-3 col-lg-2 col-xl-2 mx-auto mb-4">
                            <h6 class="text-uppercase fw-bold mb-4">
                                Useful links
                            </h6>
                            <p>
                                <a href="#!" class="text-reset">Profile</a>
                            </p>
                            <p>
                                <a href="#!" class="text-reset">All products</a>
                            </p>
                            <p>
                                <a href="#!" class="text-reset">Orders</a>
                            </p>
                            <p>
                                <a href="#!" class="text-reset">Cart</a>
                            </p>
                        </div>
                        <div class="col-md-4 col-lg-3 col-xl-3 mx-auto mb-md-0 mb-4">
                            <h6 class="text-uppercase fw-bold mb-4">Contact</h6>
                            <p>
                                <i class="fas fa-envelope me-3"></i>group6swp@outlook.com
                            </p>
                        </div>
                    </div>
                </div>
            </section>
            <div class="text-center p-4" style="background-color: rgba(0, 0, 0, 0.05);">
                © 2023 Copyright:
                <a class="text-reset fw-bold" href="http://dum1.us-east-1.elasticbeanstalk.com/">Phone Card Store</a>
            </div>
        </footer>
        <div id="notificationDiv">
            <c:if test="${not empty mess}">
                <div class="notification alert alert-${alertType} alert-dismissible fade show">
                    <button type="button" class="btn-close" data-bs-dismiss="alert"></button>
                    ${mess}
                </div>
                <c:remove var="mess" scope="session" />
                <c:remove var="alertType" scope="session" />
            </c:if>
        </div>
    </body>
</html>
