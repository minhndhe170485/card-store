<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%-- 
    Document   : home
    Created on : May 14, 2023, 2:43:21 AM
    Author     : Minh Nguyen
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <title>Products</title>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"></script>
        <script src="https://kit.fontawesome.com/59b37168e9.js" crossorigin="anonymous"></script>
        <script src="/js/myjs.js"></script>
        <link rel="stylesheet" href="/css/mycss.css"/>
        <script>
            var loadingTrow = "<tr><td><div class=\"line\"></div></td><td><div class=\"line\"></div></td><td><div class=\"line\"></div></td><td><div class=\"line\"></div></td><td><div class=\"line\"></div></td><td><div class=\"line\"></div></td><td><div class=\"line\"></div></td><td><div class=\"line\"></div></td></tr>";
            var loadingSupTbody = "<tr><td><div class=\"line\"></div><td><div class=\"line\"></div><td><div class=\"line\"></div><td><div class=\"line\"></div></tr>"
            $(document).ready(function () {
                var loadingTbody = loadingTrow.repeat($("#recPerPage").find(":selected").val());
                $("#cardTable tbody").html(loadingTbody);
                $.get("/api/admin/card", {requestStr: "list", limit: $("#recPerPage").find(":selected").val(), pageNumber: "1"}, function (responseJSON) {
                    $("#cardTable tbody").html(null);
                    var len = responseJSON.length;
                    for (var i = 0; i < len; i++) {
                        var tdId = "<td>" + responseJSON[i].id + "</td>";
                        var tdTitle = "<td>" + responseJSON[i].title + "</td>";
                        var tdDescription = "<td>" + responseJSON[i].description + "</td>";
                        var tdSupplier = "<td>" + responseJSON[i].supplier + "</td>";
                        var tdPrice = "<td>" + responseJSON[i].price + "₫</td>";
                        var tdQuantity = "<td>" + responseJSON[i].quantity + "</td>";
                        var tdAddedBy = "<td>" + responseJSON[i].createdBy + "</td>";
                        var tdAction = "<td class=\"fixed-column\" style=\"background-color: rgb(255,255,255)\"><button id=\"deleteBtn\" type = \"button\" class=\"btn btn-danger btn-sm\" data-bs-toggle=\"modal\" data-bs-target=\"#myModal\" value=\"" + responseJSON[i].id + "\">DELETE</button></td>";
                        var tr_str =
                                "<tr>" +
                                tdId +
                                tdTitle +
                                tdDescription +
                                tdSupplier +
                                tdPrice +
                                tdQuantity +
                                tdAddedBy +
                                tdAction +
                                "</tr>";
                        $("#cardTable tbody").append(tr_str);
                    }
                });
                $.get("/api/admin/card", {requestStr: "numofpage", limit: $("#recPerPage").find(":selected").val()}, function (response) {
                    var totalPages = parseInt(response, 10);
                    for (var i = 1; i <= totalPages; i++)
                    {
                        if (i === 1)
                        {
                            var li_str = "<li class=\"page-item active\"><a class=\"page-link\">" + i.toString() + "</a></li>";
                        } else
                        {
                            var li_str = "<li class=\"page-item\"><a class=\"page-link\">" + i.toString() + "</a></li>";
                        }
                        $("#pagination").append(li_str);
                    }
                });
                $("#supTable tbody").html(loadingSupTbody);
                $.get("/api/card", {requestStr: "supList"}, function (responseJSON) {
                    $("#supTable tbody").html(null);
                    var len = responseJSON.length;
                    for (var i = 0; i < len; i++)
                    {
                        var tdId = "<td>" + responseJSON[i].id + "</td>";
                        var tdName = "<td>" + responseJSON[i].name + "</td>";
                        var tdImage = "<td class=\"py-1\"><img src=\"" + responseJSON[i].image + "\" alt=\"image\"</td>";
                        var tdAction = "<td class=\"fixed-column\" style=\"background-color: rgb(255,255,255)\"><button id=\"deleteBtn1\" type = \"button\" class=\"btn btn-danger btn-sm\" data-bs-toggle=\"modal\" data-bs-target=\"#myModal1\" value=\"" + responseJSON[i].id + "\">DELETE</button></td>";
                        var tr_str =
                                "<tr>" +
                                tdId +
                                tdName +
                                tdImage +
                                tdAction +
                                "</tr>";
                        $("#supTable tbody").append(tr_str);
                        if (i === 0)
                        {
                            var op_str = "<option selected value =\"" + responseJSON[i].id + "\">" + responseJSON[i].name + "</option>";
                        } else
                        {
                            var op_str = "<option value =\"" + responseJSON[i].id + "\">" + responseJSON[i].name + "</option>";
                        }
                        $("#supIdInput").append(op_str);
                    }
                });
            });
            $(document).on("click", "#pagination li", function () {
                $("#pagination li.active").removeClass("active");
                $(this).addClass("active");
                var loadingTbody = loadingTrow.repeat($("#recPerPage").find(":selected").val());
                $("#cardTable tbody").html(loadingTbody);
                $.get("/api/admin/card", {requestStr: "list", limit: $("#recPerPage").find(":selected").val(), pageNumber: $(this).find("a").text()}, function (responseJSON) {
                    $("#cardTable tbody").html(null);
                    var len = responseJSON.length;
                    for (var i = 0; i < len; i++) {
                        var tdId = "<td>" + responseJSON[i].id + "</td>";
                        var tdTitle = "<td>" + responseJSON[i].title + "</td>";
                        var tdDescription = "<td>" + responseJSON[i].description + "</td>";
                        var tdSupplier = "<td>" + responseJSON[i].supplier + "</td>";
                        var tdPrice = "<td>" + responseJSON[i].price + "₫</td>";
                        var tdQuantity = "<td>" + responseJSON[i].quantity + "</td>";
                        var tdAddedBy = "<td>" + responseJSON[i].createdBy + "</td>";
                        var tdAction = "<td class=\"fixed-column\" style=\"background-color: rgb(255,255,255)\"><button id=\"deleteBtn\" type = \"button\" class=\"btn btn-danger btn-sm\" data-bs-toggle=\"modal\" data-bs-target=\"#myModal\" value=\"" + responseJSON[i].id + "\">DELETE</button></td>";
                        var tr_str =
                                "<tr>" +
                                tdId +
                                tdTitle +
                                tdDescription +
                                tdSupplier +
                                tdPrice +
                                tdQuantity +
                                tdAddedBy +
                                tdAction +
                                "</tr>";
                        $("#cardTable tbody").append(tr_str);
                    }
                });
            });
            $(document).on("click", "#deleteBtn", function () {
                //$(this).parent().parent().remove();
                $("#modalID").text($(this).val());
                $("#confirmBtnUID").val($(this).val());
            });
            $(document).on("click", "#deleteBtn1", function () {
                //$(this).parent().parent().remove();
                $("#modalID1").text($(this).val());
                $("#confirmBtnUID1").val($(this).val());
            });
            $(document).on("click", "#confirmBtnUID", function () {
                var loadingTbody = loadingTrow.repeat($("#recPerPage").find(":selected").val());
                $("#cardTable tbody").html(loadingTbody);
                $.get("/api/admin/card", {requestStr: "deleteCard", deleteCardId: $(this).val()}, function (responseHTML) {
                    $("#notificationDiv").html(responseHTML);
                    $.get("/api/admin/card", {requestStr: "list", limit: $("#recPerPage").find(":selected").val(), pageNumber: "1"}, function (responseJSON) {
                        $("#cardTable tbody").html(null);
                        var len = responseJSON.length;
                        for (var i = 0; i < len; i++) {
                            var tdId = "<td>" + responseJSON[i].id + "</td>";
                            var tdTitle = "<td>" + responseJSON[i].title + "</td>";
                            var tdDescription = "<td>" + responseJSON[i].description + "</td>";
                            var tdSupplier = "<td>" + responseJSON[i].supplier + "</td>";
                            var tdPrice = "<td>" + responseJSON[i].price + "₫</td>";
                            var tdQuantity = "<td>" + responseJSON[i].quantity + "</td>";
                            var tdAddedBy = "<td>" + responseJSON[i].createdBy + "</td>";
                            var tdAction = "<td class=\"fixed-column\" style=\"background-color: rgb(255,255,255)\"><button id=\"deleteBtn\" type = \"button\" class=\"btn btn-danger btn-sm\" data-bs-toggle=\"modal\" data-bs-target=\"#myModal\" value=\"" + responseJSON[i].id + "\">DELETE</button></td>";
                            var tr_str =
                                    "<tr>" +
                                    tdId +
                                    tdTitle +
                                    tdDescription +
                                    tdSupplier +
                                    tdPrice +
                                    tdQuantity +
                                    tdAddedBy +
                                    tdAction +
                                    "</tr>";
                            $("#cardTable tbody").append(tr_str);
                        }
                    });
                    $.get("/api/admin/card", {requestStr: "numofpage", limit: $("#recPerPage").find(":selected").val()}, function (response) {
                        $("#pagination").html(null);
                        var totalPages = parseInt(response, 10);
                        for (var i = 1; i <= totalPages; i++)
                        {
                            if (i === 1)
                            {
                                var li_str = "<li class=\"page-item active\"><a class=\"page-link\">" + i.toString() + "</a></li>";
                            } else
                            {
                                var li_str = "<li class=\"page-item\"><a class=\"page-link\">" + i.toString() + "</a></li>";
                            }
                            $("#pagination").append(li_str);
                        }
                    });
                });
            });
            $(document).on("click", "#confirmBtnUID1", function () {
                $("#supTable tbody").html(loadingSupTbody);
                var loadingTbody = loadingTrow.repeat($("#recPerPage").find(":selected").val());
                $("#cardTable tbody").html(loadingTbody);
                $.get("/api/admin/card", {requestStr: "deleteProvider", deleteProviderId: $(this).val()}, function (responseHTML) {
                    $("#notificationDiv").html(responseHTML);
                    $.get("/api/admin/card", {requestStr: "list", limit: $("#recPerPage").find(":selected").val(), pageNumber: "1"}, function (responseJSON) {
                        $("#cardTable tbody").html(null);
                        var len = responseJSON.length;
                        for (var i = 0; i < len; i++) {
                            var tdId = "<td>" + responseJSON[i].id + "</td>";
                            var tdTitle = "<td>" + responseJSON[i].title + "</td>";
                            var tdDescription = "<td>" + responseJSON[i].description + "</td>";
                            var tdSupplier = "<td>" + responseJSON[i].supplier + "</td>";
                            var tdPrice = "<td>" + responseJSON[i].price + "₫</td>";
                            var tdQuantity = "<td>" + responseJSON[i].quantity + "</td>";
                            var tdAddedBy = "<td>" + responseJSON[i].createdBy + "</td>";
                            var tdAction = "<td class=\"fixed-column\" style=\"background-color: rgb(255,255,255)\"><button id=\"deleteBtn\" type = \"button\" class=\"btn btn-danger btn-sm\" data-bs-toggle=\"modal\" data-bs-target=\"#myModal\" value=\"" + responseJSON[i].id + "\">DELETE</button></td>";
                            var tr_str =
                                    "<tr>" +
                                    tdId +
                                    tdTitle +
                                    tdDescription +
                                    tdSupplier +
                                    tdPrice +
                                    tdQuantity +
                                    tdAddedBy +
                                    tdAction +
                                    "</tr>";
                            $("#cardTable tbody").append(tr_str);
                        }
                    });
                    $.get("/api/admin/card", {requestStr: "numofpage", limit: $("#recPerPage").find(":selected").val()}, function (response) {
                        $("#pagination").html(null);
                        var totalPages = parseInt(response, 10);
                        for (var i = 1; i <= totalPages; i++)
                        {
                            if (i === 1)
                            {
                                var li_str = "<li class=\"page-item active\"><a class=\"page-link\">" + i.toString() + "</a></li>";
                            } else
                            {
                                var li_str = "<li class=\"page-item\"><a class=\"page-link\">" + i.toString() + "</a></li>";
                            }
                            $("#pagination").append(li_str);
                        }
                    });
                    $.get("/api/card", {requestStr: "supList"}, function (responseJSON) {
                        $("#supTable tbody").html(null);
                        var len = responseJSON.length;
                        for (var i = 0; i < len; i++)
                        {
                            var tdId = "<td>" + responseJSON[i].id + "</td>";
                            var tdName = "<td>" + responseJSON[i].name + "</td>";
                            var tdImage = "<td class=\"py-1\"><img src=\"" + responseJSON[i].image + "\" alt=\"image\"</td>";
                            var tdAction = "<td class=\"fixed-column\" style=\"background-color: rgb(255,255,255)\"><button id=\"deleteBtn1\" type = \"button\" class=\"btn btn-danger btn-sm\" data-bs-toggle=\"modal\" data-bs-target=\"#myModal1\" value=\"" + responseJSON[i].id + "\">DELETE</button></td>";
                            var tr_str =
                                    "<tr>" +
                                    tdId +
                                    tdName +
                                    tdImage +
                                    tdAction +
                                    "</tr>";
                            $("#supTable tbody").append(tr_str);
                            if (i === 0)
                            {
                                var op_str = "<option selected value =\"" + responseJSON[i].id + "\">" + responseJSON[i].name + "</option>";
                            } else
                            {
                                var op_str = "<option value =\"" + responseJSON[i].id + "\">" + responseJSON[i].name + "</option>";
                            }
                            $("#supIdInput").append(op_str);
                        }
                    });
                });
            });
            $(document).on("click", "#addBtn", function () {
                $.post("/api/admin/card", {requestStr: "addCard", title: $("#titleInput").val(), description: $("#descrInput").val(), supId: $("#supIdInput").find(":selected").val(), price: $("#priceInput").val()}, function (responseHTML) {
                    $("#notificationDiv").html(responseHTML);
                    $("#titleInput").val(null);
                    $("#descrInput").val(null);
                    $("#priceInput").val(null);
                    var loadingTbody = loadingTrow.repeat($("#recPerPage").find(":selected").val());
                    $("#cardTable tbody").html(loadingTbody);
                    $.get("/api/admin/card", {requestStr: "list", limit: $("#recPerPage").find(":selected").val(), pageNumber: "1"}, function (responseJSON) {
                        $("#cardTable tbody").html(null);
                        var len = responseJSON.length;
                        for (var i = 0; i < len; i++) {
                            var tdId = "<td>" + responseJSON[i].id + "</td>";
                            var tdTitle = "<td>" + responseJSON[i].title + "</td>";
                            var tdDescription = "<td>" + responseJSON[i].description + "</td>";
                            var tdSupplier = "<td>" + responseJSON[i].supplier + "</td>";
                            var tdPrice = "<td>" + responseJSON[i].price + "₫</td>";
                            var tdQuantity = "<td>" + responseJSON[i].quantity + "</td>";
                            var tdAddedBy = "<td>" + responseJSON[i].createdBy + "</td>";
                            var tdAction = "<td class=\"fixed-column\" style=\"background-color: rgb(255,255,255)\"><button id=\"deleteBtn\" type = \"button\" class=\"btn btn-danger btn-sm\" data-bs-toggle=\"modal\" data-bs-target=\"#myModal\" value=\"" + responseJSON[i].id + "\">DELETE</button></td>";
                            var tr_str =
                                    "<tr>" +
                                    tdId +
                                    tdTitle +
                                    tdDescription +
                                    tdSupplier +
                                    tdPrice +
                                    tdQuantity +
                                    tdAddedBy +
                                    tdAction +
                                    "</tr>";
                            $("#cardTable tbody").append(tr_str);
                        }
                    });
                    $.get("/api/admin/card", {requestStr: "numofpage", limit: $("#recPerPage").find(":selected").val()}, function (response) {
                        $("#pagination").html(null);
                        var totalPages = parseInt(response, 10);
                        for (var i = 1; i <= totalPages; i++)
                        {
                            if (i === 1)
                            {
                                var li_str = "<li class=\"page-item active\"><a class=\"page-link\">" + i.toString() + "</a></li>";
                            } else
                            {
                                var li_str = "<li class=\"page-item\"><a class=\"page-link\">" + i.toString() + "</a></li>";
                            }
                            $("#pagination").append(li_str);
                        }
                    });
                });
            });
            $(document).on("click", "#addProvBtn", function () {
                $("#supTable tbody").html(loadingSupTbody);
                $.post("/api/admin/card", {requestStr: "addProvider", name: $("#provNameInput").val(), imgUrl: $("#provImgInput").val()}, function (responseHTML) {
                    $("#notificationDiv").html(responseHTML);
                    $("#provNameInput").val(null);
                    $("#provImgInput").val(null);
                    $.get("/api/card", {requestStr: "supList"}, function (responseJSON) {
                        $("#supTable tbody").html(null);
                        $("#supIdInput").html(null);
                        var len = responseJSON.length;
                        for (var i = 0; i < len; i++)
                        {
                            var tdId = "<td>" + responseJSON[i].id + "</td>";
                            var tdName = "<td>" + responseJSON[i].name + "</td>";
                            var tdImage = "<td class=\"py-1\"><img src=\"" + responseJSON[i].image + "\" alt=\"image\"</td>";
                            var tdAction = "<td class=\"fixed-column\" style=\"background-color: rgb(255,255,255)\"><button id=\"deleteBtn1\" type = \"button\" class=\"btn btn-danger btn-sm\" data-bs-toggle=\"modal\" data-bs-target=\"#myModal1\" value=\"" + responseJSON[i].id + "\">DELETE</button></td>";
                            var tr_str =
                                    "<tr>" +
                                    tdId +
                                    tdName +
                                    tdImage +
                                    tdAction +
                                    "</tr>";
                            $("#supTable tbody").append(tr_str);
                            if (i === 0)
                            {
                                var op_str = "<option selected value =\"" + responseJSON[i].id + "\">" + responseJSON[i].name + "</option>";
                            } else
                            {
                                var op_str = "<option value =\"" + responseJSON[i].id + "\">" + responseJSON[i].name + "</option>";
                            }
                            $("#supIdInput").append(op_str);
                        }
                    });
                });
            });
            $(document).on("change", "#recPerPage", function () {
                //var optionSelected = $("option:selected", this);
                var valueSelected = this.value;
                var loadingTbody = loadingTrow.repeat($("#recPerPage").find(":selected").val());
                $("#cardTable tbody").html(loadingTbody);
                $.get("/api/admin/card", {requestStr: "list", limit: valueSelected, pageNumber: "1"}, function (responseJSON) {
                    $("#cardTable tbody").html(null);
                    var len = responseJSON.length;
                    for (var i = 0; i < len; i++) {
                        var tdId = "<td>" + responseJSON[i].id + "</td>";
                        var tdTitle = "<td>" + responseJSON[i].title + "</td>";
                        var tdDescription = "<td>" + responseJSON[i].description + "</td>";
                        var tdSupplier = "<td>" + responseJSON[i].supplier + "</td>";
                        var tdPrice = "<td>" + responseJSON[i].price + "₫</td>";
                        var tdQuantity = "<td>" + responseJSON[i].quantity + "</td>";
                        var tdAddedBy = "<td>" + responseJSON[i].createdBy + "</td>";
                        var tdAction = "<td class=\"fixed-column\" style=\"background-color: rgb(255,255,255)\"><button id=\"deleteBtn\" type = \"button\" class=\"btn btn-danger btn-sm\" data-bs-toggle=\"modal\" data-bs-target=\"#myModal\" value=\"" + responseJSON[i].id + "\">DELETE</button></td>";
                        var tr_str =
                                "<tr>" +
                                tdId +
                                tdTitle +
                                tdDescription +
                                tdSupplier +
                                tdPrice +
                                tdQuantity +
                                tdAddedBy +
                                tdAction +
                                "</tr>";
                        $("#cardTable tbody").append(tr_str);
                    }
                });
                $.get("/api/admin/card", {requestStr: "numofpage", limit: valueSelected}, function (response) {
                    $("#pagination").html(null);
                    var totalPages = parseInt(response, 10);
                    for (var i = 1; i <= totalPages; i++)
                    {
                        if (i === 1)
                        {
                            var li_str = "<li class=\"page-item active\"><a class=\"page-link\">" + i.toString() + "</a></li>";
                        } else
                        {
                            var li_str = "<li class=\"page-item\"><a class=\"page-link\">" + i.toString() + "</a></li>";
                        }
                        $("#pagination").append(li_str);
                    }
                });
            });
        </script>
    </head>
    <body>
        <div class="modal fade" id="myModal">
            <div class="modal-dialog">
                <div class="modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">Confirm Deletion</h4>
                        <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body">
                        <p>Are you sure you want to delete this card with ID = <span id="modalID"></span>?</p>
                    </div>

                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light" data-bs-dismiss="modal">Cancel</button>
                        <button type="button" class="btn btn-danger" data-bs-dismiss="modal" id="confirmBtnUID">Delete</button>
                    </div>

                </div>
            </div>
        </div>
        <div class="modal fade" id="myModal1">
            <div class="modal-dialog">
                <div class="modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">Confirm Deletion</h4>
                        <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body">
                        <p>Are you sure you want to delete this provider with ID = <span id="modalID1"></span>?</p>
                    </div>

                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light" data-bs-dismiss="modal">Cancel</button>
                        <button type="button" class="btn btn-danger" data-bs-dismiss="modal" id="confirmBtnUID1">Delete</button>
                    </div>

                </div>
            </div>
        </div>
        <nav class="navbar navbar-expand-sm bg-dark navbar-dark" id="navbar">
            <div class="container">
                <a class="navbar-brand" href="/">Phone Card Store</a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#collapsibleNavbar">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="collapsibleNavbar">
                    <ul class="navbar-nav">
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown">PROVIDER</a>
                            <ul class="dropdown-menu">
                                <li><a class="dropdown-item" href="#">Viettel</a></li>
                                <li><a class="dropdown-item" href="#">Vinaphone</a></li>
                                <li><a class="dropdown-item" href="#">MobiFone</a></li>
                                <li><a class="dropdown-item" href="#">Vietnamobile</a></li>
                            </ul>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown">PRICE</a>
                            <ul class="dropdown-menu">
                                <li><a class="dropdown-item" href="#">10.000₫</a></li>
                                <li><a class="dropdown-item" href="#">20.000₫</a></li>
                                <li><a class="dropdown-item" href="#">50.000₫</a></li>
                                <li><a class="dropdown-item" href="#">100.000₫</a></li>
                                <li><a class="dropdown-item" href="#">200.000₫</a></li>
                                <li><a class="dropdown-item" href="#">500.000₫</a></li>
                            </ul>
                        </li>
                    </ul>
                    <ul class="navbar-nav ms-auto">
                        <c:if test="${not empty sessionScope.account}">
                            <span class="navbar-text me-1">${sessionScope.account.getBalance()}₫</span>
                        </c:if>
                        <c:if test="${sessionScope.account.getIsAdmin()}">
                            <li class="nav-item">
                                <a class="nav-link" href="/admin"><i class="fa-solid fa-gear"></i></i></a>
                            </li>
                        </c:if>
                        <li class="nav-item">
                            <a class="nav-link" href="/profile"><i class="fa-solid fa-circle-user"></i></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/orders"><i class="fa-solid fa-cart-shopping"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <main class="container-fluid page-body-wrapper">
            <nav class="sidebar sidebar-offcanvas" id="sidebar">
                <ul class="nav">
                    <li class="nav-item nav-profile">
                        <a href="/profile" class="nav-link">
                            <i class="fa-solid fa-circle-user" style="font-size: 40px;"></i>
                            <div class="nav-profile-text d-flex flex-column">
                                <span class="fs-4 fw-bold mb-2">${sessionScope.account.getUserName()}</span>
                                <span class="fs-6 text-small">Admin</span>
                            </div>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/admin">
                            <span class="menu-title">Dashboard</span>

                            <i class="fa-solid fa-house menu-icon"></i>
                        </a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" data-bs-toggle="collapse" href="#product-nav" aria-expanded="true"
                           aria-controls="product-nav">
                            <span class="menu-title">Products</span>
                            <i class="fa-solid menu-arrow "></i>
                            <i class="fa-solid fa-box-archive menu-icon"></i>
                        </a>
                        <div class="collapse show" id="product-nav">
                            <ul class="nav flex-column sub-menu">
                                <li class="nav-item "> <a class="nav-link active" href="/admin/product">Product list</a></li>
                                <li class="nav-item "> <a class="nav-link" href="/admin/product/detail">Product detail</a></li>
                            </ul>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/admin/user">
                            <span class="menu-title">Users</span>
                            <i class="fa-solid fa-users menu-icon"></i>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/admin/order">
                            <span class="menu-title">Orders</span>
                            <i class="fa-solid fa-cart-shopping menu-icon"></i>
                        </a>
                    </li>
                </ul>
            </nav>
            <div class="main-panel">
                <div class="content-wrapper">
                    <div class="page-header">
                        <h3 class="page-title">
                            <span class="page-title-icon bg-gradient-primary text-white me-2">
                                <i class="fa-solid fa-users"></i>
                            </span> Card Management
                        </h3>
                    </div>
                    <div class="col-lg-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Card</h4>
                                <p class="card-description"> View and update phone cards
                                </p>
                                <div class="table-responsive">
                                    <table class="table table-hover" id="cardTable">
                                        <thead>
                                            <tr>
                                                <th> ID </th>
                                                <th> Title </th>
                                                <th> Description </th>
                                                <th> Provider </th>
                                                <th> Price </th>
                                                <th> Stock </th>
                                                <th> Added by (UID) </th>
                                                <th class="fixed-column" style="background-color: rgb(255,255,255)"> Action </th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                    </table>
                                </div>
                                <div class="mt-3">
                                    <div class="float-start">
                                        <label for="recPerPage" class="form-label">Rows per page: </label>
                                        <select class="form-select ms-auto" id="recPerPage">
                                            <option selected value="5">5</option>
                                            <option value="10">10</option>
                                            <option value="15">15</option>
                                        </select>
                                    </div>
                                    <div class="float-end">
                                        <ul class="pagination" id="pagination">

                                        </ul>
                                    </div>
                                </div>
                                <br>
                                <br>
                                <h4 class="card-title mt-5">Add Card</h4>
                                <p class="card-description"> Adding new card here
                                </p>
                                <div class="table-responsive">
                                    <table class="table table-hover" id="">
                                        <thead>
                                            <tr>
                                                <th> Title </th>
                                                <th> Description </th>
                                                <th> Provider </th>
                                                <th> Price </th>
                                                <th class="fixed-column" style="background-color: rgb(255,255,255)"> Action </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td> <input class="form-control" type="text" id="titleInput"> </td>
                                                <td> <input class="form-control" type="text" id="descrInput"> </td>
                                                <td> 
                                                    <select class="form-select ms-auto" id="supIdInput">

                                                    </select> 
                                                </td>
                                                <td> <input class="form-control" type="number" id="priceInput"> </td>
                                                <td><button id="addBtn" type = "button" class="btn btn-success btn-sm" >ADD</button></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="page-header">
                        <h3 class="page-title">
                            <span class="page-title-icon bg-gradient-primary text-white me-2">
                                <i class="fa-solid fa-globe"></i>
                            </span> Provider Management
                        </h3>
                    </div>
                    <div class="col-lg-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Provider</h4>
                                <p class="card-description"> View and update provider list
                                </p>
                                <div class="table-responsive">
                                    <table class="table table-hover" id="supTable">
                                        <thead>
                                            <tr>
                                                <th> ID </th>
                                                <th> Name </th>
                                                <th> Image </th>
                                                <th class="fixed-column" style="background-color: rgb(255,255,255)"> Action </th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                    </table>
                                </div>
                                <h4 class="card-title mt-5">Add Provider</h4>
                                <p class="card-description"> Adding new provider here
                                </p>
                                <div class="table-responsive">
                                    <table class="table table-hover" id="">
                                        <thead>
                                            <tr>
                                                <th> Name </th>
                                                <th> Image link </th>
                                                <th class="fixed-column" style="background-color: rgb(255,255,255)"> Action </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td> <input class="form-control" type="text" id="provNameInput"> </td>
                                                <td> <input class="form-control" type="text" id="provImgInput"> </td>
                                                <td><button id="addProvBtn" type = "button" class="btn btn-success btn-sm" >ADD</button></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <br>
                        </div>
                    </div>
                </div>
            </div>       
        </main>
        <footer class="text-center text-lg-start bg-light text-muted">
            <section class="d-flex justify-content-center justify-content-lg-between p-4 border-bottom">
                <div class="me-5 d-none d-lg-block">
                    <span>Get connected with us on social networks:</span>
                </div>
                <div>
                    <a href="https://www.facebook.com/duc.minh.2911/" target="_blank" class="me-4 text-reset">
                        <i class="fab fa-facebook-f"></i>
                    </a>
                </div>
            </section>
            <section class="">
                <div class="container text-center text-md-start mt-5">
                    <div class="row mt-3">
                        <div class="col-md-3 col-lg-4 col-xl-3 mx-auto mb-4">
                            <h6 class="text-uppercase fw-bold mb-4">
                                <i class="fas fa-gem me-3"></i>Phone Card Store
                            </h6>
                            <p>
                                Made by group 6.
                            </p>
                        </div>
                        <div class="col-md-2 col-lg-2 col-xl-2 mx-auto mb-4">
                            <h6 class="text-uppercase fw-bold mb-4">
                                Provider
                            </h6>
                            <p>
                                <a href="#!" class="text-reset">Viettel</a>
                            </p>
                            <p>
                                <a href="#!" class="text-reset">Vinaphone</a>
                            </p>
                            <p>
                                <a href="#!" class="text-reset">MobiFone</a>
                            </p>
                            <p>
                                <a href="#!" class="text-reset">Vietnamobile</a>
                            </p>
                        </div>
                        <div class="col-md-3 col-lg-2 col-xl-2 mx-auto mb-4">
                            <h6 class="text-uppercase fw-bold mb-4">
                                Useful links
                            </h6>
                            <p>
                                <a href="#!" class="text-reset">Profile</a>
                            </p>
                            <p>
                                <a href="#!" class="text-reset">All products</a>
                            </p>
                            <p>
                                <a href="#!" class="text-reset">Orders</a>
                            </p>
                            <p>
                                <a href="#!" class="text-reset">Cart</a>
                            </p>
                        </div>
                        <div class="col-md-4 col-lg-3 col-xl-3 mx-auto mb-md-0 mb-4">
                            <h6 class="text-uppercase fw-bold mb-4">Contact</h6>
                            <p>
                                <i class="fas fa-envelope me-3"></i>group6swp@outlook.com
                            </p>
                        </div>
                    </div>
                </div>
            </section>
            <div class="text-center p-4" style="background-color: rgba(0, 0, 0, 0.05);">
                © 2023 Copyright:
                <a class="text-reset fw-bold" href="http://dum1.us-east-1.elasticbeanstalk.com/">Phone Card Store</a>
            </div>
        </footer>
        <div id="notificationDiv">
            <c:if test="${not empty mess}">
                <div class="notification alert alert-${alertType} alert-dismissible fade show">
                    <button type="button" class="btn-close" data-bs-dismiss="alert"></button>
                    ${mess}
                </div>
                <c:remove var="mess" scope="session" />
                <c:remove var="alertType" scope="session" />
            </c:if>
        </div>
    </body>
</html>
