<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%-- 
    Document   : home
    Created on : May 14, 2023, 2:43:21 AM
    Author     : Minh Nguyen
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Orders</title>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"></script>
        <script src="https://kit.fontawesome.com/59b37168e9.js" crossorigin="anonymous"></script>
        <script src="/js/myjs.js"></script>
        <link rel="stylesheet" href="/css/mycss.css"/>
        <script>
            $(document).on("click", "#showBtn", function () {
                //$(this).parent().parent().remove();
                $("#cardTbody").html(null);
                const jsonCards = JSON.parse($(this).val());
                //$("#modalText").html('<pre style="color: #DAFFFB; background: #001C30; padding: 10px; border-radius: 10px;">' + JSON.stringify(JSON.parse($(this).val()), null, 2).replace(/"([^"]+)"\s*:/g, '<span style="color: #64CCC5;">"$1":</span>').replace(/:\s*"([^"]+)"/g, ': <span style="color: green;">"$1"</span>').replace(/\{/g, '<span style="color: white;">{</span>').replace(/\}/g, '<span style="color: white;">}</span>').replace(/\[/g, '<span style="color: white;">[</span>').replace(/\]/g, '<span style="color: white;">]</span>')
                       // + "</pre>");
                for(var i =0;i<jsonCards.length;i++)
                {
                    var tdSeri = "<td>" +  jsonCards[i].seriNumber + "</td>";
                    var tdCode = "<td>" +  jsonCards[i].code + "</td>";
                    var tdExpire = "<td>" +  jsonCards[i].productExpireDate + "</td>";
                    var tr_str = "<tr>" + tdSeri + tdCode + tdExpire + "</tr>";
                    $("#cardTbody").append(tr_str);
                }
            });
        </script>

    </head>
    <body>
        <div class="modal fade" id="myModal">
            <div class="modal-dialog">
                <div class="modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">Product detail</h4>
                        <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body">
<!--                        <div id="modalText"></div>-->
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Serial number</th>
                                    <th>Card number</th>
                                    <th>Expiration time</th>
                                </tr>
                            </thead>
                            <tbody id="cardTbody">

                            </tbody>   
                        </table>
                    </div>

                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Close</button>
                    </div>

                </div>
            </div>
        </div>
        <nav class="navbar navbar-expand-sm bg-dark navbar-dark" id="navbar">
            <div class="container">
                <a class="navbar-brand" href="/">Phone Card Store</a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#collapsibleNavbar">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="collapsibleNavbar">
                    <ul class="navbar-nav">
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown">PROVIDER</a>
                            <ul class="dropdown-menu">
                                <li><a class="dropdown-item" href="#">Viettel</a></li>
                                <li><a class="dropdown-item" href="#">Vinaphone</a></li>
                                <li><a class="dropdown-item" href="#">MobiFone</a></li>
                                <li><a class="dropdown-item" href="#">Vietnamobile</a></li>
                            </ul>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown">PRICE</a>
                            <ul class="dropdown-menu">
                                <li><a class="dropdown-item" href="#">10.000₫</a></li>
                                <li><a class="dropdown-item" href="#">20.000₫</a></li>
                                <li><a class="dropdown-item" href="#">50.000₫</a></li>
                                <li><a class="dropdown-item" href="#">100.000₫</a></li>
                                <li><a class="dropdown-item" href="#">200.000₫</a></li>
                                <li><a class="dropdown-item" href="#">500.000₫</a></li>
                            </ul>
                        </li>
                    </ul>
                    <ul class="navbar-nav ms-auto">
                        <c:if test="${not empty sessionScope.account}">
                            <span class="navbar-text me-1">${sessionScope.account.getBalance()}₫</span>
                        </c:if>
                        <c:if test="${sessionScope.account.getIsAdmin()}">
                            <li class="nav-item">
                                <a class="nav-link" href="/admin"><i class="fa-solid fa-gear"></i></i></a>
                            </li>
                        </c:if>
                        <li class="nav-item">
                            <a class="nav-link" href="/profile"><i class="fa-solid fa-circle-user"></i></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/orders"><i class="fa-solid fa-cart-shopping"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <main >
            <section>
                <div class="container mt-5 ">
                    <div class="form__container text-center">
                        <div class="text-center form__container-header">
                            <h3><a style="font-size: 20px;color: grey" href="/profile">MY PROFILE</a> | MY ORDERS</h3>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th> Order ID </th>
                                    <th> Product Title </th>
                                    <th> Total Price </th>
                                    <th> Product Detail </th>
                                </tr>
                            </thead>
                            <tbody>
                                <c:forEach var="ol" items="${orderList}">
                                    <tr>
                                        <td>${ol.getId()}</td>
                                        <td>${ol.getProductTitle()}</td>
                                        <td>${ol.getTotalPrice()}₫</td>
                                        <td><button id="showBtn" type = "button" class="btn btn-info btn-sm" data-bs-toggle="modal" data-bs-target="#myModal" value='${ol.getProductLog()}'>SHOW</button></td>
                                    </tr>
                                </c:forEach>
                            </tbody>
                        </table>
                    </div>
                </div>
            </section>
        </main>
        <footer class="text-center text-lg-start bg-light text-muted">
            <section class="d-flex justify-content-center justify-content-lg-between p-4 border-bottom">
                <div class="me-5 d-none d-lg-block">
                    <span>Get connected with us on social networks:</span>
                </div>
                <div>
                    <a href="https://www.facebook.com/duc.minh.2911/" target="_blank" class="me-4 text-reset">
                        <i class="fab fa-facebook-f"></i>
                    </a>
                </div>
            </section>
            <section class="">
                <div class="container text-center text-md-start mt-5">
                    <div class="row mt-3">
                        <div class="col-md-3 col-lg-4 col-xl-3 mx-auto mb-4">
                            <h6 class="text-uppercase fw-bold mb-4">
                                <i class="fas fa-gem me-3"></i>Phone Card Store
                            </h6>
                            <p>
                                Made by group 6.
                            </p>
                        </div>
                        <div class="col-md-2 col-lg-2 col-xl-2 mx-auto mb-4">
                            <h6 class="text-uppercase fw-bold mb-4">
                                Provider
                            </h6>
                            <p>
                                <a href="#!" class="text-reset">Viettel</a>
                            </p>
                            <p>
                                <a href="#!" class="text-reset">Vinaphone</a>
                            </p>
                            <p>
                                <a href="#!" class="text-reset">MobiFone</a>
                            </p>
                            <p>
                                <a href="#!" class="text-reset">Vietnamobile</a>
                            </p>
                        </div>
                        <div class="col-md-3 col-lg-2 col-xl-2 mx-auto mb-4">
                            <h6 class="text-uppercase fw-bold mb-4">
                                Useful links
                            </h6>
                            <p>
                                <a href="#!" class="text-reset">Profile</a>
                            </p>
                            <p>
                                <a href="#!" class="text-reset">All products</a>
                            </p>
                            <p>
                                <a href="#!" class="text-reset">Orders</a>
                            </p>
                            <p>
                                <a href="#!" class="text-reset">Cart</a>
                            </p>
                        </div>
                        <div class="col-md-4 col-lg-3 col-xl-3 mx-auto mb-md-0 mb-4">
                            <h6 class="text-uppercase fw-bold mb-4">Contact</h6>
                            <p>
                                <i class="fas fa-envelope me-3"></i>
                                group6swp@outlook.com
                            </p>
                        </div>
                    </div>
                </div>
            </section>
            <div class="text-center p-4" style="background-color: rgba(0, 0, 0, 0.05);">
                © 2023 Copyright:
                <a class="text-reset fw-bold" href="http://dum1.us-east-1.elasticbeanstalk.com/">Phone Card Store</a>
            </div>
        </footer>
        <div id="notificationDiv">
            <c:if test="${not empty mess}">
                <div class="notification alert alert-${alertType} alert-dismissible fade show">
                    <button type="button" class="btn-close" data-bs-dismiss="alert"></button>
                    ${mess}
                </div>
                <c:remove var="mess" scope="session" />
                <c:remove var="alertType" scope="session" />
            </c:if>
        </div>
    </body>
</html>
