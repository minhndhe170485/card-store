/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.util.Date;

/**
 *
 * @author Minh Nguyen
 */
public class OrderProcessing
{
    private int id;
    private int supplier_id;
    private int price;
    private int quantity;
    private int account_id;
    //Constructor
    public OrderProcessing(){
    
    }
    public OrderProcessing(int id, int supplier_id, int price, int quantity, int account_id)
    {
        this.id=id;
        this.supplier_id=supplier_id;
        this.price=price;
        this.quantity=quantity;
        this.account_id=account_id;
    }
    //Setter
    public void setId(int id)
    {
        this.id=id;
    }
    public void setSupplierId(int supplier_id)
    {
        this.supplier_id=supplier_id;
    }
    public void setPrice(int price)
    {
        this.price=price;
    }
    public void setQuantity(int quantity)
    {
        this.quantity=quantity;
    }
    public void setAccountId(int account_id)
    {
        this.account_id=account_id;
    }
    //Getter
    public int getId()
    {
        return this.id;
    }
    public int getSupplierId()
    {
        return this.supplier_id;
    }
    public int getPrice()
    {
        return this.price;
    }
    public int getQuantity()
    {
        return this.quantity;
    }
    public int getAccountId()
    {
        return this.account_id;
    }
}
