/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.util.Date;

/**
 *
 * @author Minh Nguyen
 */
public class Order 
{
    private int id;
    private String product_title;
    private int createdBy;
    private String productLog;
    private int totalPrice;
    //Constructor
    public Order(){
    
    }
    public Order(int id, String product_title, int createdBy, String productLog, int totalPrice)
    {
        this.id=id;
        this.product_title=product_title;
        this.createdBy=createdBy;
        this.productLog=productLog;
        this.totalPrice=totalPrice;
    }
    //Setter
    public void setId(int id)
    {
        this.id=id;
    }
    public void setProductTitle(String product_title)
    {
        this.product_title=product_title;
    }
    public void setCreatedBy(int createdBy)
    {
        this.createdBy=createdBy;
    }
    public void setProductLog(String productLog)
    {
        this.productLog=productLog;
    }
    public void setTotalPrice(int totalPrice)
    {
        this.totalPrice=totalPrice;
    }
    //Getter
    public int getId()
    {
        return this.id;
    }
    public String getProductTitle()
    {
        return this.product_title;
    }
    public int getCreatedBy()
    {
        return this.createdBy;
    }
    public String getProductLog()
    {
        return this.productLog;
    }
    public int getTotalPrice()
    {
        return this.totalPrice;
    } 
}
