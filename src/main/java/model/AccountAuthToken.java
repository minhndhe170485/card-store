/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author Minh Nguyen
 */
public class AccountAuthToken 
{
    private int id;
    private String selector;
    private String validator;
    private int accountId;
    public AccountAuthToken()
    {
        
    }
    public AccountAuthToken(int id,String selector,String validator, int accountId)
    {
        this.id=id;
        this.selector=selector;
        this.validator=validator;
        this.accountId=accountId;
    }
    public void setId(int id)
    {
        this.id=id;
    }
    public void setSelector(String selector)
    {
        this.selector=selector;
    }
    public void setValidator(String validator)
    {
        this.validator=validator;
    }
    public void setAccountId(int accountId)
    {
        this.accountId=accountId;
    }
    public int getId()
    {
        return this.id;
    }
    public String getSelector()
    {
        return this.selector;
    }
    public String getValidator()
    {
        return this.validator;
    }
    public int getAccountId()
    {
        return this.accountId;
    }
}
