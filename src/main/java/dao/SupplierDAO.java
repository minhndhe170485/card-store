/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import static dao.DBContext.getConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import model.Supplier;


public class SupplierDAO extends DBContext
{
     public Supplier getSupplierById(String pid) {
        Connection conn = getConnection();
        ResultSet rs = null;
        PreparedStatement statement = null;
        Supplier p = new Supplier();
        try 
        {
            String sql = " Select * from supplier where id = " + pid;
            statement = conn.prepareStatement(sql);
            rs = statement.executeQuery();
            while (rs.next()) 
            {
                p = new Supplier(rs.getInt(1),rs.getString(2),rs.getString(3));
            }
        } 
        catch (Exception e) 
        {
            e.printStackTrace();
        }
        finally 
        {
            closeConnection(rs, statement, conn);
        }
        return p;
    }
     
     public ArrayList<Supplier> getAllSupplier() {
        Connection conn = getConnection();
        ResultSet rs = null;
        PreparedStatement statement = null;
        ArrayList<Supplier> list = new ArrayList<>();
        try 
        {
            String sql = "Select * from supplier";
            statement = conn.prepareStatement(sql);
            rs = statement.executeQuery();
            while (rs.next()) {
                Supplier p = new Supplier(rs.getInt(1), rs.getString(2), rs.getString(3));
                list.add(p);
            }
        } 
        catch (Exception e) 
        {
            e.printStackTrace();
        }
        finally 
        {
            closeConnection(rs, statement, conn);
        }
        return list;
    }
     
    public boolean deleteProvider(String id)
    {
        Connection conn = getConnection();
        ResultSet rs = null;
        PreparedStatement statement = null;
        int a=0;
        ProductDAO prodDao = new ProductDAO();
        try 
        {
            String sql0 = "select id from product WHERE (supplier = ?)";
            statement = conn.prepareStatement(sql0);
            statement.setString(1, id);
            rs = statement.executeQuery();
            while (rs.next()) {
                prodDao.deleteCard(Integer.toString(rs.getInt(1)));
            }
            String sql = "delete from supplier WHERE (id = ?)";
            statement = conn.prepareStatement(sql);
            statement.setString(1, id);
            a = statement.executeUpdate();
            
        }
        catch (Exception e) 
        {
            e.printStackTrace();
        }
        finally 
        {
            closeConnection(rs, statement, conn);
        }
        if(a==0)
        {
            return false;
        }
        return true;
    }
    public boolean addProvider(String name, String imgUrl)
    {
        Connection conn = getConnection();
        ResultSet rs = null;
        PreparedStatement statement = null;
        int a = 0;
        try {
            String sql = "insert into supplier (name, image) values (?,?)";
            statement = conn.prepareStatement(sql);
            statement.setString(1, name);
            statement.setString(2, imgUrl);
            a = statement.executeUpdate();
        } 
        catch (Exception e) 
        {
            e.printStackTrace();
        }
        finally 
        {
            closeConnection(rs, statement, conn);
        }
        if(a==0)
        {
            return false;
        }
        return true;
    }
}
