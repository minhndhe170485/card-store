/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import static dao.DBContext.closeConnection;
import static dao.DBContext.getConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import model.OrderProcessing;
import model.Product;

/**
 *
 * @author Minh Nguyen
 */
public class OrderProcessingDAO {
    public void addOrderProcessing(int supId, int price, int quantity, int accId) {
        Connection conn = getConnection();
        ResultSet rs = null;
        PreparedStatement statement = null;
        try {
            String sql = "INSERT INTO `order_processing` (supplierId, price, quantity, accountId) VALUES (?,?,?,?)";
            statement = conn.prepareStatement(sql);
            statement.setInt(1, supId);
            statement.setInt(2, price);
            statement.setInt(3, quantity);
            statement.setInt(4, accId);
            statement.executeUpdate();
        } 
        catch (Exception e) 
        {
            e.printStackTrace();
        }
        finally 
        {
            closeConnection(rs, statement, conn);
        }
    }
    public OrderProcessing getTop()
    {
        Connection conn = getConnection();
        ResultSet rs = null;
        PreparedStatement statement = null;
        OrderProcessing op = new OrderProcessing();
        try {
            String sql = "select * from order_processing limit 1";
            statement = conn.prepareStatement(sql);
            rs = statement.executeQuery();
            while (rs.next()) {
                op = new OrderProcessing(rs.getInt(1), rs.getInt(2), rs.getInt(3), rs.getInt(4), rs.getInt(5));
            }
        } 
        catch (Exception e) 
        {
            e.printStackTrace();
        }
        finally 
        {
            closeConnection(rs, statement, conn);
        }
        return op;
    }
    public void deleteTop()
    {
        Connection conn = getConnection();
        ResultSet rs = null;
        PreparedStatement statement = null;
        try {
            String sql = "DELETE FROM order_processing LIMIT 1";
            statement = conn.prepareStatement(sql);
            statement.executeUpdate();
        } 
        catch (Exception e) 
        {
            e.printStackTrace();
        }
        finally 
        {
            closeConnection(rs, statement, conn);
        }
    }
    public int numberOfRecs()
    {
        Connection conn = getConnection();
        ResultSet rs = null;
        PreparedStatement statement = null;
        int n=0;
        try {
            String sql = "select count(id) FROM order_processing";
            statement = conn.prepareStatement(sql);
            rs = statement.executeQuery();
            while (rs.next()) {
                n = rs.getInt(1);
            }
        } 
        catch (Exception e) 
        {
            e.printStackTrace();
        }
        finally 
        {
            closeConnection(rs, statement, conn);
        }
        return n;
    }
}
