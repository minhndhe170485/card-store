/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import static dao.DBContext.getConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Random;
import model.AccountAuthToken;
import org.apache.commons.codec.digest.DigestUtils;
/**
 *
 * @author Minh Nguyen
 */
public class AccountAuthDAO extends DBContext 
{
    public AccountAuthToken getBySelector(String selector)
    {
        Connection conn = getConnection();
        ResultSet rs = null;
        PreparedStatement statement = null;
        AccountAuthToken token = null;
        try 
        {
            String sql = "select id,validator,account_id from account_auth where selector = ?";
            statement = conn.prepareStatement(sql);
            statement.setString(1, selector);
            rs = statement.executeQuery();
            while (rs.next()) 
            {
                token = new AccountAuthToken(rs.getInt(1),selector,rs.getString(2),rs.getInt(3));
                break;
            }
        } 
        catch (Exception e) 
        {
            e.printStackTrace();
        }
        finally 
        {
            closeConnection(rs, statement, conn);
        }
        return token;
    }
    public void update(String oldSelector,AccountAuthToken token)
    {
        Connection conn = getConnection();
        ResultSet rs = null;
        PreparedStatement statement = null;
        try {
            String validatorHash = DigestUtils.sha256Hex(token.getValidator());
            String sql = "update account_auth set selector = ?, validator = ? where (selector = ?)";
            statement = conn.prepareStatement(sql);
            statement.setString(1, token.getSelector());
            statement.setString(2, validatorHash);
            statement.setString(3, oldSelector);
            statement.executeUpdate();
        } 
        catch (Exception e) 
        {
            e.printStackTrace();
        }
        finally 
        {
            closeConnection(rs, statement, conn);
        }
    }
    public void create(AccountAuthToken token) 
    {
        Connection conn = getConnection();
        ResultSet rs = null;
        PreparedStatement statement = null;
        try 
        {
            String validatorHash = DigestUtils.sha256Hex(token.getValidator());
            String sql = "insert into account_auth (selector, validator, account_id) values (?,?,?)";
            statement = conn.prepareStatement(sql);
            statement.setString(1, token.getSelector());
            statement.setString(2, validatorHash);
            statement.setInt(3, token.getAccountId());
            statement.executeUpdate();
        } 
        catch (Exception e) 
        {
            e.printStackTrace();
        }
        finally 
        {
            closeConnection(rs, statement, conn);
        }
    }
    public void delete(int id) 
    {
        Connection conn = getConnection();
        ResultSet rs = null;
        PreparedStatement statement = null;
        try 
        {
            String sql = "delete from account_auth WHERE (id = ?);";
            statement = conn.prepareStatement(sql);
            statement.setInt(1, id);
            statement.executeUpdate();
        } 
        catch (Exception e) 
        {
            e.printStackTrace();
        }
        finally 
        {
            closeConnection(rs, statement, conn);
        }
    }
}
