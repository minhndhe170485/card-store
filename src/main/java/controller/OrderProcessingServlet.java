/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import com.google.gson.Gson;
import dao.AccountDAO;
import dao.OrderDAO;
import dao.OrderProcessingDAO;
import dao.ProductDAO;
import dao.ProductDetailDAO;
import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.Account;
import model.OrderProcessing;
import model.ProductDetail;

/**
 *
 * @author Minh Nguyen
 */
public class OrderProcessingServlet extends HttpServlet {

    private ScheduledExecutorService executor;

    @Override
    public void init() throws ServletException {
        executor = Executors.newSingleThreadScheduledExecutor();
        executor.scheduleAtFixedRate(new OrderProcessingTask(), 0, 5, TimeUnit.SECONDS);
    }

    @Override
    public void destroy() {
        if (executor != null && !executor.isShutdown()) {
            executor.shutdown();
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // Handle GET requests if needed
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // Handle POST requests if needed
    }
}

class OrderProcessingTask implements Runnable {

    @Override
    public void run() {
        OrderProcessingDAO OPDao = new OrderProcessingDAO();
        // Logic to continuously check and process pending orders from the processing_order table
        // Perform necessary actions such as deducting balance, updating inventory, generating logs, etc.
        // Remove processed orders from the processing_order table
        while (OPDao.numberOfRecs() > 0) {
            OrderProcessing op = OPDao.getTop();

            AccountDAO accDao = new AccountDAO();
            ProductDAO proDao = new ProductDAO();
            ProductDetailDAO proDDao = new ProductDetailDAO();
            OrderDAO orDao = new OrderDAO();

            Account acc = accDao.getProfileByID(op.getAccountId());

            String supId = Integer.toString(op.getSupplierId());
            int price = op.getPrice();
            int quantity = op.getQuantity();
            int totalPrice = price * quantity;
            if (quantity > proDao.getQuantityBySupIdAndPrice(supId, price)) 
            {
                //do nothing
            } else if (acc.getBalance() < totalPrice) 
            {
                //do nothing
            } 
            else 
            {
                int PId = proDao.getIdBySupIdAndPrice(supId, price);
                accDao.setBalanceByID(acc.getId(), acc.getBalance() - totalPrice);
                ArrayList<ProductDetail> list = proDDao.getCardsDetailWithPIDAndLimit(Integer.toString(quantity), PId);
                Object[] objects = list.toArray();
                Gson gson = new Gson();
                String productLog = gson.toJson(objects);
                orDao.addOrder(PId, acc.getId(), productLog, totalPrice);
            }
            OPDao.deleteTop();
        }
    }
}
