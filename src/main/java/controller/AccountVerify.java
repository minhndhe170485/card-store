/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dao.AccountDAO;
import dao.DBContext;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.Account;

/**
 *
 * @author Minh Nguyen
 */
public class AccountVerify extends HttpServlet{

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet AccountActivate</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet AccountActivate at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
		
	String email = request.getParameter("key1");
	String hash = request.getParameter("key2");
		
	Connection con = DBContext.getConnection();
		
	PreparedStatement pstmt = null;
        
        ResultSet rs = null;
	HttpSession session = request.getSession();
        try {
            pstmt = con.prepareStatement("SELECT verifyExpire FROM account WHERE email=? AND verifyKey=? AND isVerified='0'");
            pstmt.setString(1, email);
            pstmt.setString(2, hash);
            
            rs = pstmt.executeQuery();
            if (rs.next()){
                Date expire = rs.getTimestamp(1);
                Date current = new Date();
                if(current.compareTo(expire) <= 0) 
                {
                    request.setAttribute("txtemail", email);
                    AccountDAO dao = new AccountDAO();
                    dao.verify(email);
                    /*Account curdao = (Account) session.getAttribute("account");
                    if(curdao!=null&&curdao.getEmail().equals(email))
                    {
                        Account acc = dao.getProfile(email);
                        session.setAttribute("account", acc);
                    }*/
                    DBContext.closeConnection(rs, pstmt, con);
                    session.setAttribute("mess", "Account has been verified.");
                    session.setAttribute("alertType", "success");
                    response.sendRedirect("profile");
                } 
                else
                {
                    DBContext.closeConnection(rs, pstmt, con);
                    session.setAttribute("mess", "Your verify token is expired.");
                    session.setAttribute("alertType", "danger");
                    response.sendRedirect("/");
                } 
            }
            else
            {
                DBContext.closeConnection(rs, pstmt, con);
                session.setAttribute("mess", "Invalid Link.");
                session.setAttribute("alertType", "danger");
                response.sendRedirect("/");
            }
        } 
        catch (Exception e) 
        {
            e.printStackTrace();
        }
    }
    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
