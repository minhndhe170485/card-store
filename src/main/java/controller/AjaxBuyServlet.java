/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import com.google.gson.Gson;
import dao.AccountDAO;
import dao.OrderDAO;
import dao.OrderProcessingDAO;
import dao.ProductDAO;
import dao.ProductDetailDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.Account;
import model.ProductDetail;

/**
 *
 * @author Minh Nguyen
 */
public class AjaxBuyServlet extends HttpServlet {
    private static final Object lock = new Object();
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet BuyServlet</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet BuyServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        /*synchronized (lock)
        {
            HttpSession session=request.getSession();
            AccountDAO accDao = new AccountDAO();
            ProductDAO proDao = new ProductDAO();
            ProductDetailDAO proDDao = new ProductDetailDAO();
            OrderDAO orDao = new OrderDAO();
            Account acc = (Account)session.getAttribute("account");
            boolean ajax = "XMLHttpRequest".equals(request.getHeader("X-Requested-With"));
            if(acc==null)
            {
                if(ajax)
                {
                    response.setContentType("text/plain");
                    response.getWriter().write("Not logged in.");
                }
                else{
                    session.setAttribute("mess", "Not logged in.");
                    session.setAttribute("alertType", "danger");
                    response.sendRedirect("/login");
                }
            }
            else{
                acc = accDao.getProfileByID(acc.getId());
                String supId = request.getParameter("supId");
                int price = Integer.parseInt(request.getParameter("price"));
                int quantity = Integer.parseInt(request.getParameter("quantity"));
                int totalPrice = price*quantity;
                response.setContentType("text/html");
                if(quantity>proDao.getQuantityBySupIdAndPrice(supId,price))
                {
                    response.getWriter().write(responseNotiHTML("Out of stock", "danger"));
                }
                else if(acc.getBalance()<totalPrice)
                {
                    response.getWriter().write(responseNotiHTML("Insufficient balance", "danger"));
                }
                else
                {
                    int PId = proDao.getIdBySupIdAndPrice(supId, price);
                    accDao.setBalanceByID(acc.getId(),acc.getBalance()-totalPrice);
                    ArrayList<ProductDetail> list = proDDao.getCardsDetailWithPIDAndLimit(Integer.toString(quantity), PId);
                    Object[] objects = list.toArray();
                    Gson gson = new Gson();
                    String productLog = gson.toJson(objects);
                    orDao.addOrder(PId, acc.getId(), productLog, totalPrice);
                    response.getWriter().write(responseNotiHTML("Purchase successful, please open the <a href=\"/orders\">purchase history page</a>.", "success"));
                }
            }
        }*/
        HttpSession session = request.getSession();
        OrderProcessingDAO opDao = new OrderProcessingDAO();
        Account acc = (Account) session.getAttribute("account");
        boolean ajax = "XMLHttpRequest".equals(request.getHeader("X-Requested-With"));
        if (acc == null) {
            if (ajax) {
                response.setContentType("text/plain");
                response.getWriter().write("Not logged in.");
            } else {
                session.setAttribute("mess", "Not logged in.");
                session.setAttribute("alertType", "danger");
                response.sendRedirect("/login");
            }
        } else {
            int accId = acc.getId();
            String supId = request.getParameter("supId");
            int price = Integer.parseInt(request.getParameter("price"));
            int quantity = Integer.parseInt(request.getParameter("quantity"));
            opDao.addOrderProcessing(Integer.parseInt(supId), price, quantity, accId);
            response.setContentType("text/html");
            response.getWriter().write(responseNotiHTML("Your order has been added to queue.", "success"));
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
    private String responseNotiHTML(String mess, String alertType)
    {
        String html=    "<div class=\"notification alert alert-"+ alertType +" alert-dismissible fade show\">\n"+
                                "<button type=\"button\" class=\"btn-close\" data-bs-dismiss=\"alert\"></button>\n" +
                                mess + "\n" +
                        "</div>";
        return html;
    }
}
