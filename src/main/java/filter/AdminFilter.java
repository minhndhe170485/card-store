/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package filter;

/**
 *
 * @author Minh Nguyen
 */
import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import javax.servlet.http.HttpSession;
import model.Account;

public class AdminFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        // Initialization code, if needed
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {

        HttpServletRequest httpRequest = (HttpServletRequest) request;
        HttpServletResponse httpResponse = (HttpServletResponse) response;
        boolean ajax = "XMLHttpRequest".equals(httpRequest.getHeader("X-Requested-With"));
        // Check if the user is an admin
        boolean isAdmin = checkIfUserIsAdmin(httpRequest);

        if (isAdmin) {
            // User is an admin, allow access to the resource
            chain.doFilter(request, response);
        } else {
            if(ajax)
            {
                httpResponse.setContentType("application/json");
                httpResponse.getWriter().write("[]");
            }
            else{
                // User is not an admin, redirect to an error page or display an error message
                HttpSession session = httpRequest.getSession();
                session.setAttribute("mess", "Access denied.");
                session.setAttribute("alertType", "danger");
                httpResponse.sendRedirect("/");
            }
        }
    }

    @Override
    public void destroy() {
        // Cleanup code, if needed
    }

    private boolean checkIfUserIsAdmin(HttpServletRequest request) {
        // Implement your logic to check if the user is an admin
        // You can use session attributes, database lookups, or any other means of determining the user's role
        // For example, if you have a session attribute named "isAdmin" set to true for admin users:
        Account curacc = (Account) request.getSession().getAttribute("account");
        if(curacc == null) return false;
        Boolean isAdmin = curacc.getIsAdmin();
        return isAdmin;
    }
}
